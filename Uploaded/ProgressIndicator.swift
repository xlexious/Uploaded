/*
    Copyright (c) 2016, Alex S. Glomsaas
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

        1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

        3. Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import Foundation
import Cocoa

protocol ProgressIndicator {
    var progressStrokeColor: CGColor { get }
    var inderminateStrokeColor: CGColor { get }
    
    func drawDefault(_ frame: NSRect) -> CALayer
    func drawSelected(_ frame: NSRect) -> CALayer
    func drawProgress(_ percentage: Double, frame: NSRect) -> CAShapeLayer
    func drawInderminate(_ frame: NSRect) -> CAShapeLayer
}

extension ProgressIndicator {
    func drawDefault(_ frame: NSRect) -> CALayer {
        let image = NSImage.fromAssetCatalogue("menu")
        let layer = CALayer()
        layer.frame = frame
        layer.contents = image
        return layer
    }
    
    func drawSelected(_ frame: NSRect) -> CALayer {
        let image = NSImage.fromAssetCatalogue("menu_selected")
        let layer = CALayer()
        layer.frame = frame
        layer.contents = image
        return layer
    }
    
    func drawProgress(_ percentage: Double, frame: NSRect) -> CAShapeLayer {
        let circleSize = CGPoint(x: frame.size.width, y: frame.size.height)
        let calculatedStartPosition = CGFloat(90 + (360 - percentage * 3.6))
        let calculatedArcSize = CGFloat(90)
        
        let path = NSBezierPath()
        path.appendArc(withCenter: circleSize, radius: 7, startAngle: calculatedStartPosition, endAngle: calculatedArcSize, clockwise: false)
        
        let progressLayer = CAShapeLayer()
        progressLayer.bounds      = frame
        progressLayer.path        = path.cgPath
        progressLayer.strokeColor = progressStrokeColor
        progressLayer.fillColor   = NSColor.clear.cgColor
        progressLayer.lineWidth   = 3
        
        return progressLayer
    }
    
    func drawInderminate(_ frame: NSRect) -> CAShapeLayer {
        let circleSize = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        
        let path = NSBezierPath()
        path.appendArc(withCenter: circleSize, radius: 7, startAngle: 90, endAngle: 0, clockwise: true)
        
        let progressLayer = CAShapeLayer()
        progressLayer.bounds      = frame
        progressLayer.path        = path.cgPath
        progressLayer.strokeColor = inderminateStrokeColor
        progressLayer.fillColor   = NSColor.clear.cgColor
        progressLayer.lineWidth   = 3
        progressLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        progressLayer.frame       = frame
        
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.duration            = 1.0
        animation.isRemovedOnCompletion = false
        animation.fromValue           = 0
        animation.toValue             = CGFloat(-M_PI * 2)
        animation.repeatCount         = .infinity
        
        progressLayer.add(animation, forKey: nil)
        return progressLayer
    }
}

struct LightProgressIndicator: ProgressIndicator {
    var progressStrokeColor = NSColor(red: 0.29, green: 0.53, blue: 0.38, alpha: 1.0).cgColor
    var inderminateStrokeColor = NSColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0).cgColor
}

struct DarkProgressIndicator: ProgressIndicator {
    var progressStrokeColor = NSColor(red: 0.24, green: 0.77, blue: 0.44, alpha: 1.0).cgColor
    var inderminateStrokeColor = NSColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).cgColor
    
    func drawDefault(_ frame: NSRect) -> CALayer {
        let image = NSImage.fromAssetCatalogue("menu_selected")
        let layer = CALayer()
        layer.frame = frame
        layer.contents = image
        return layer
    }
    
    func drawSelected(_ frame: NSRect) -> CALayer {
        return drawDefault(frame)
    }
}

