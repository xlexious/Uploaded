/*
    Copyright (c) 2016, Alex S. Glomsaas
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

        1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

        3. Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import Foundation

class SFTPFileUploadOperation: UploadOperation, FileUpload {
    private let filePath: URL
    private let fileSize: Int64?
    
    required init(filePath: URL, fileAttributes: Dictionary<FileAttributeKey, Any>) {
        self.filePath = filePath
        let fileSizeValue = fileAttributes[.size] as! NSNumber
        self.fileSize = fileSizeValue.int64Value
    }
    
    override func main() {
        if self.isCancelled {
            return
        }
        
        let session = NMSSHSession.connect(toHost: "sorlie.co.uk", port: 3139, withUsername: "alex")
        if ((session?.isConnected) != nil) {
            //FIXME: Remember to change
            session?.authenticate(byPassword: "4LuH7?eqKE76RH4fjKGjD*%GuRoroUJpew(")
            if ((session?.isAuthorized) != nil) {
                print("Authentication successful")
            }
        }
        
        if (self.fileSize != nil) {
            self.progress.totalUnitCount = self.fileSize!
            print(self.fileSize ?? "No filesize")
        } else {
            print("nil filesize")
        }
        
        let success = session?.channel.uploadFile(filePath.path, to: "/var/www/html/www_sorlie_co_uk/", progress: {
            (progress: UInt) -> Bool in
            
            self.progress.completedUnitCount = Int64(progress)
            return true
        })
        
        self.url = URL(string: "https://sorlie.co.uk/\(filePath.lastPathComponent)")
        
        session?.disconnect()
    }
    
    override func cancel() {
        
    }
}
