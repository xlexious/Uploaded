/*
    Copyright (c) 2016, Alex S. Glomsaas
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

        1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

        3. Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import Foundation
import Cocoa

class ProgressIndicatorView: NSView, CALayerDelegate {
    @IBOutlet var viewController: MenuController!
    
    var active = false
    
    required override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.wantsLayer = true
        self.layer = CALayer()
        self.layer?.delegate = self
        self.nextResponder = viewController
        self.register(forDraggedTypes: [kUTTypeData as String])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.wantsLayer = true
        self.layer = self.makeBackingLayer()
        self.layer?.delegate = self
        self.nextResponder = viewController
        self.register(forDraggedTypes: [kUTTypeData as String])
    }
    
    
    override func draw(_ dirtyRect: NSRect) {
        if (self.needsToDraw(dirtyRect)) {
            if (self.active) {
                NSColor.selectedMenuItemColor.set()
                NSRectFill(dirtyRect)
            } else {
                NSColor.clear.set()
                NSRectFill(dirtyRect)
            }
        }
    }
    
    override func mouseDown(with theEvent: NSEvent) {
        self.active = !self.active
        self.needsDisplay = true
        viewController.mouseDown(with: theEvent)
    }
    
    override var nextResponder: NSResponder? {
        get {
            return super.nextResponder
        }
        
        set {
            if (self.viewController != nil) {
                viewController.nextResponder = nextResponder
                return
            }
            super.nextResponder = nextResponder
        }
    }
    
    
    
    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        NSLog("Entered")
        return .copy
    }
    
    override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let pasteboard = sender.draggingPasteboard()
        
        if (pasteboard.types?.contains(NSFilenamesPboardType) == true) {
            if let files = pasteboard.propertyList(forType: NSFilenamesPboardType) as? [String] {
                viewController.performDragOperation(files)
                return true
            }
            
        }
        
        return false
    }
}
