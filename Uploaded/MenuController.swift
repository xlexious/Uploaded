/*
    Copyright (c) 2016, Alex S. Glomsaas
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

        1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

        3. Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import Foundation
import Cocoa

class MenuController: NSViewController, NSMenuDelegate, UploaderDelegate {
    var statusItem: NSStatusItem?
    @IBOutlet var statusMenu: NSMenu!
    var progressIndicator: ProgressIndicator
    let uploader = Uploader()
    
    
    required init?(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, statusItem: NSStatusItem) {
        self.progressIndicator = NSAppearance.currentTheme == .dark ? DarkProgressIndicator() : LightProgressIndicator()
        
        self.statusItem = statusItem
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.uploader.delegate = self
    }
    
    override init?(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.progressIndicator = NSAppearance.currentTheme == .dark ? DarkProgressIndicator() : LightProgressIndicator()
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    
    override func viewDidLoad() {
        CATransaction.begin()
        //let progress = self.progressIndicator.drawProgress(75, frame: NSRect(x: 0, y: 0, width: 18, height: 18))
        //let progress = self.progressIndicator.drawInderminate(NSRect(x: 0, y: 0, width: 24, height: 18))
        let applicationIcon = self.progressIndicator.drawDefault(NSRect(x: 2, y: 2, width: 18, height: 18))
        
        self.icon = applicationIcon
        CATransaction.commit()
        
        
    }
    
    var icon: CALayer? {
        get {
            return self.view.layer!.sublayers?[0]
        }
        
        set (iconLayer) {
            if (iconLayer != nil) {
                self.view.layer!.sublayers?.removeAll()
                iconLayer?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                self.view.layer!.addSublayer(iconLayer!)
            } else {
                self.view.layer!.sublayers?.removeAll()
            }
            
            let indicatorView = self.view as! ProgressIndicatorView
            indicatorView.needsDisplay = true
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func menuDidClose(_ menu: NSMenu) {
        let indicatorView = self.view as! ProgressIndicatorView
        indicatorView.active = false
        indicatorView.needsDisplay = true
    }
    
    override func mouseDown(with theEvent: NSEvent) {
        self.statusItem?.popUpMenu(statusMenu)
    }
    
    func setIconProgress(progress: Double) {
        let progressLayer = self.progressIndicator.drawProgress(progress, frame: NSRect(x: 0, y: 0, width: 18, height: 18))
        CATransaction.begin()
        self.icon = progressLayer
        CATransaction.commit()
    }
    
    func performDragOperation(_ files: [String]) {
        Swift.print(files)
        for file in files {
            if let fileUrl = URL(string: file) {
                uploader.upload(fileUrl: fileUrl)
            }
        }
    }
    
    func finishedUpload(sender: Uploader) {
        print("Finished")
    }
    
    func uploadReceivedProgress(progress: Double, sender: Uploader) {
        self.setIconProgress(progress: progress)
        print("progress: \(progress)")
    }
}
